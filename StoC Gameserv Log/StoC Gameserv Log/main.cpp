#include "ConsoleEx.h"
#include <cstring>

ConsoleEx* con;

template <typename T>
class gw_array
{
protected:
	T* arr_;
	DWORD allocatedsize_;
	DWORD currentsize_;
	DWORD unk_;
public:
	inline T& operator[](DWORD index) { return arr_[index]; }
	inline const DWORD& size() const { return currentsize_; }
};

struct PacketBase
{
	DWORD header;
};

struct P147_UpdateGenericValue : public PacketBase {
	DWORD type;
	DWORD AgentID;
	DWORD value;
};

template <typename T>
using StoCHandler				= bool(__fastcall*)(T* packet);
using PacketParameterTemplate	= DWORD*;

struct CtoSTableIndex {
	PacketParameterTemplate params;
	DWORD param_count;
};

template <class PakStruct_t>
struct StoCTableIndex {
	PacketParameterTemplate params;
	DWORD param_count;
	StoCHandler<PakStruct_t> handler;
}; 

using CtoSTable					= gw_array<CtoSTableIndex>;

class StoCTable : public gw_array<StoCTableIndex<PacketBase>> {
public:
	template <class PakStruct_t>
	inline StoCTableIndex<PakStruct_t>& GetHandler(DWORD header)
	{
		return ((StoCTableIndex<PakStruct_t>*)arr_)[header];
	}
};


class GameServer 
{
	GameServer(){}
	GameServer(const GameServer&){}

	using PacketSender_t = void(__fastcall*)(void* thisptr, DWORD size, void* data);

public:
	BYTE pad1[0x8];
	struct GameServer_sub1
	{
		BYTE pad1[0xC];
		struct LSConsts
		{
			BYTE pad1[0x1C];
			CtoSTable ctos_table;
			StoCTable stoc_table;
		} *loginsrv_consts;
		BYTE pad2[0xC];
		CtoSTable ctos_table;
		StoCTable stoc_table;
	} *consts; //?

	static GameServer* instance()
	{
		static GameServer* inst = nullptr;
		if (inst == nullptr){
			const BYTE scancode[] = { 0x56, 0x33, 0xF6, 0x3B, 0xCE, 0x74, 0x0E, 0x56, 0x33, 0xD2 };

			for (BYTE* start = (BYTE*)0x401000; start < (BYTE*)0x900000; ++start)
			{
				if (!memcmp(start, scancode, sizeof(scancode)))
				{
					inst = **(GameServer***)(start - 4);
					break;
				}
			}
		}
		return inst;
	}

	void SendPacket(DWORD size, void* data)
	{
		static PacketSender_t sendpak = (PacketSender_t)0x58E130;
		sendpak(this, size, data);
	}
};

bool stop = false;

StoCTableIndex<PacketBase>* origFuncs = nullptr;
DWORD value = 0;
bool __fastcall stoc_handler(PacketBase* pak)
{
	if (!stop){
		con->printf("[New GS] Header: %d\n", pak->header);
		if (pak->header == 147)
		{
			P147_UpdateGenericValue* newpak = (P147_UpdateGenericValue*)pak;
			if (newpak->type == 0x1A)
			{
				newpak->value = value;
			}
		}
	}
	return origFuncs[pak->header].handler(pak);
}



bool __fastcall val_handler(P147_UpdateGenericValue* pak)
{
	
	return origFuncs[pak->header].handler(pak);
}

void input(std::string string)
{
	char cstr[255];
	strcpy(cstr,string.c_str());
	if (!strcmp(strtok(cstr," "),"/famerank"))
	{
		value = atoi(strtok(NULL, " "));
		con->printf("Fame rank now %d.\n", value);
	}
	else if (!strcmp(strtok(cstr, " "), "/stop"))
	{
		stop = !stop;
	}
}

void init(HMODULE hModule){

	CONSOLEEX_PARAMETERS params = CONSOLEEX_PARAMETERS();

	params.cellsX = 40;
	params.closemenu = true;
	params.cellsYBuffer = 5000;

	con = ConsoleEx::GenerateLogger(hModule, "[StoC] GameServer", params);

	con->registerInputCallback(input);

	con->SetPos(HWND_TOPMOST, 50, 50);

	// Get GameServer Object
	GameServer* srv = GameServer::instance();

	if (srv == nullptr)
	{
		delete con;
		FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
	}

	// Retrieve GameServer table.
	StoCTable table = srv->consts->stoc_table;

	// Create restore buffer
	origFuncs = new StoCTableIndex<PacketBase>[table.size()];

	// C memcpy should be more efficient than iteration through table.
	memcpy(origFuncs, *(void**)&table, sizeof(StoCTableIndex<PacketBase>) * table.size());

	for (DWORD i = 0; i < table.size(); ++i)
	{
		table[i].handler = stoc_handler;
	}

	while (con->isOpen()) Sleep(100);


	memcpy(*(void**)&table, origFuncs, sizeof(StoCTableIndex<PacketBase>) * table.size());
	delete con;
	FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);

}

BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved){
	if (_Reason == DLL_PROCESS_ATTACH){
		DisableThreadLibraryCalls(_HDllHandle);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
	}
	return TRUE;
}