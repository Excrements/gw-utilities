#include <Windows.h>
#include <iostream>

char txtfilePath[MAX_PATH];
HANDLE htxtfile;


void *DetourFunc(BYTE *src, const BYTE *dst, const int len, BYTE** restore = NULL)
{

	if (restore){
		*restore = new BYTE[len];
		memcpy(*restore, src, len);
	}


	BYTE *jmp = (BYTE*)malloc(len + 5);
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_READWRITE, &dwBack);

	memcpy(jmp, src, len);
	jmp += len;

	jmp[0] = 0xE9;
	*(DWORD*)(jmp + 1) = (DWORD)(src + len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src + 1) = (DWORD)(dst - src) - 5;

	for (int i = 5; i < len; i++)
		src[i] = 0x90;

	VirtualProtect(src, len, dwBack, &dwBack);

	return (jmp - len);
}

void RetourFunc(BYTE *src, BYTE *restore, const int len)
{
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_READWRITE, &dwBack);
	memcpy(src, restore, len);

	restore[0] = 0xE9;
	*(DWORD*)(restore + 1) = (DWORD)(src - restore) - 5;

	VirtualProtect(src, len, dwBack, &dwBack);

	delete[] restore;
}

struct s_UpdatePos{
	DWORD header;
	DWORD AgentId;
	float X;
	float Y;
	DWORD Zplane;
};


typedef bool(__fastcall *UpdatePos_t)(s_UpdatePos* pak, DWORD unk);
UpdatePos_t returnfunc = NULL;
BYTE* restore = NULL;

char buf[128];


bool __fastcall UpdatePosHandler(s_UpdatePos* pak, DWORD unk)
{
	DWORD bytesread;
	htxtfile = CreateFileA(txtfilePath, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);

	memset(buf, 0, 128);

	if (ReadFile(htxtfile, buf, 128, &bytesread, NULL))
	{
		char* tok = NULL;

		tok = strtok(buf, " ");
		if (tok != NULL){
			pak->X = atof(tok);
		}

		tok = strtok(NULL, " ");
		if (tok != NULL){
			pak->Y = atof(tok);
		}

		tok = strtok(NULL, " ");
		if (tok != NULL){
			pak->Zplane = atoi(tok);
		}
	}
	CloseHandle(htxtfile);

	return returnfunc(pak, unk);
}

void init(HMODULE hModule){

	GetModuleFileNameA(hModule, txtfilePath, sizeof(txtfilePath));
	std::string::size_type pos = std::string(txtfilePath).find_last_of("\\/");

	std::string buffer = std::string(txtfilePath).substr(0, pos + 1);
	strcpy_s(txtfilePath, buffer.c_str());
	strcat_s(txtfilePath, "coords.txt");

	returnfunc = (UpdatePos_t)DetourFunc((BYTE*)0x05D41D5, (BYTE*)UpdatePosHandler, 6, &restore);


	while (1){
		Sleep(100);
		if (GetAsyncKeyState(VK_END) & 1)
		{
			RetourFunc((BYTE*)0x05D41D5, restore, 6);
			FreeLibraryAndExitThread(hModule, 0);
		}
	}

}


BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved){
	if (_Reason == DLL_PROCESS_ATTACH){
		DisableThreadLibraryCalls(_HDllHandle);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
	}
	return TRUE;
}