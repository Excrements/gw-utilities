#include <Windows.h>
#include <stdio.h>
/*
 * Guild Wars Multiclient Patch by 4D 1
 * Credits to ACB for all of the reversing and patch code.
 * http://www.gamerevision.com for GW bots n shit.
 */

// PatternScan Class
// By four dee one, methods mostly copypasta
class PatternScan{

	BYTE* m_buf;
	DWORD m_size;
	DWORD m_start;

	bool DataCompare(BYTE* CodeMask,char* StringMask, byte* DataToCompare, int pos){
		for (; *StringMask; ++pos, ++CodeMask, ++StringMask){
			if (*StringMask == 'x' && *CodeMask != (byte)DataToCompare[pos]){
				return false;
			}
		}
		return true;
	}

public:
	PatternScan(HANDLE proc, DWORD start, DWORD size)
	: m_start(start), m_size(size){
		m_buf = new BYTE[m_size];
		ReadProcessMemory(proc, (LPCVOID)m_start, m_buf, m_size, NULL);
	}
	~PatternScan()
	{
		delete[] m_buf;
	}

	BYTE* ScanFor(BYTE* CodeMask,char* StringMask, int offset = NULL)
	{
		for (DWORD i = 0; i < m_size; i++)
		{
			if (DataCompare(CodeMask, StringMask, m_buf, i))
			{
				return (BYTE*)(m_start + i + offset);
			}
		}
		return NULL;
	}


};

BOOL WriteMem(HANDLE hProc,byte* pAddress, byte* pBuffer, int iSize){
	DWORD dwOldProtection;
	VirtualProtectEx(hProc, pAddress, sizeof(pBuffer), PAGE_READWRITE, &dwOldProtection);
	BOOL ret = WriteProcessMemory(hProc, pAddress, pBuffer, iSize, NULL);
	VirtualProtectEx(hProc, pAddress, sizeof(pBuffer), dwOldProtection, &dwOldProtection);
	return ret;
}

int CALLBACK WinMain(
	_In_  HINSTANCE hInstance,
	_In_  HINSTANCE hPrevInstance,
	_In_  LPSTR lpCmdLine,
	_In_  int nCmdShow
	)
{
	bool doDATFix = 0;
	CHAR buf[5];
	CHAR INIPath[MAX_PATH];
	CHAR DLLName[100];


	DWORD szPath = GetModuleFileNameA(NULL, INIPath, MAX_PATH);

	while (INIPath[szPath] != '\\') szPath--;
	INIPath[szPath + 1] = '\0';
	strcat_s(INIPath, MAX_PATH, "GWMC.ini");


	GetPrivateProfileStringA("GWMC", "DATFix", "0", buf, 5, INIPath);
	sscanf_s(buf, "%u", &doDATFix);


	// Set Regkey values to this client.
	HKEY locMachineKey;

	WCHAR gwExeFullPath[MAX_PATH];

	GetFullPathNameW(L"Gw.exe", 255, gwExeFullPath, NULL);

	if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\ArenaNet\\Guild Wars", NULL, KEY_ALL_ACCESS, &locMachineKey) == ERROR_SUCCESS){
		RegSetValueEx(locMachineKey, L"Src", NULL, REG_SZ, (PBYTE)gwExeFullPath, (wcslen(gwExeFullPath) + 1)*sizeof(WCHAR));
		RegSetValueEx(locMachineKey, L"Path", NULL, REG_SZ, (PBYTE)gwExeFullPath, (wcslen(gwExeFullPath) + 1)*sizeof(WCHAR));
	}
	else if (RegOpenKeyEx(HKEY_LOCAL_MACHINE, L"SOFTWARE\\Wow6432Node\\ArenaNet\\Guild Wars", NULL, KEY_ALL_ACCESS, &locMachineKey) == ERROR_SUCCESS){
		RegSetValueEx(locMachineKey, L"Src", NULL, REG_SZ, (PBYTE)gwExeFullPath, (wcslen(gwExeFullPath) + 1)*sizeof(WCHAR));
		RegSetValueEx(locMachineKey, L"Path", NULL, REG_SZ, (PBYTE)gwExeFullPath, (wcslen(gwExeFullPath) + 1)*sizeof(WCHAR));
	}



	STARTUPINFOA 		   startupInfo;
	PROCESS_INFORMATION    processInformation;

	ZeroMemory(&startupInfo, sizeof(STARTUPINFO));
	startupInfo.cb = sizeof(STARTUPINFO);
	ZeroMemory(&processInformation, sizeof(PROCESS_INFORMATION));

	//Start GW
	if (CreateProcessA("Gw.exe", GetCommandLineA(), NULL, NULL, false, CREATE_SUSPENDED, NULL, NULL, &startupInfo, &processInformation) == FALSE){
		MessageBoxA(0, "Unable to find Gw.exe\nMake sure this exe is in your Guild Wars directory.", "GWMC - Err", 0);
		return -1;
	}

	// Initialize scanner for functions in memory.
	PatternScan* scan = new PatternScan(processInformation.hProcess, 0x401000, 0x4FF000);



	// MC PATCH
	BYTE* MCPatchAddr = scan->ScanFor((PBYTE)"\x75\x07\xC7\x45\xFC\xFF\xFF\xFF\xFF\x85\xF6\x75\xFF\xFF\xD7\x50\x68","xxxxx????xxx?xxxx");
	if (!MCPatchAddr){
		MessageBoxA(0, "MC Patch Addr not found.", "GWMC - Err", 0);
		delete scan;
		return -1;
	}

	BYTE* MCbuf = new BYTE;
	*MCbuf = (BYTE)0xEB;

	if (!WriteMem(processInformation.hProcess, MCPatchAddr, MCbuf, 1)){
		MessageBoxA(0, "Could not Write MC Patch.", "GWMC - Err", 0);
		delete scan;
		delete MCbuf;
		return -1;
	}
	delete MCbuf;

	//Dat Fix #1
	if (doDATFix){
		BYTE* DatFix1 = scan->ScanFor((PBYTE)"\x6A\x00\xBA\x00\x00\x00\xC0", "xxxxxxx", 1);
		if (!DatFix1){
			MessageBoxA(0, "Dat Fix #1 not found.", "GWMC - Err", 0);
			delete scan;
			return -1;
		}

		BYTE* Dat1Buf = new BYTE;
		*Dat1Buf = (BYTE)0x03;

		if (!WriteMem(processInformation.hProcess, DatFix1, Dat1Buf, 1)){
			MessageBoxA(0, "Could not Write Dat Fix #1.", "GWMC - Err", 0);
			delete scan;
			delete Dat1Buf;
			return -1;
		}
		delete Dat1Buf;

		// Dat Fix #2

		BYTE* DatFix2 = scan->ScanFor((PBYTE)"\x8D\x55\x0C\x6A\x00\x52\x57\x50\x51", "xxxxxxxxx", 3);
		if (!DatFix2){
			MessageBoxA(0, "Dat fix #2 not found.", "GWMC - Err", 0);
			delete scan;
			return -1;
		}

		BYTE* Dat2Buf = new BYTE[12];

		for (int i = 0; i < 12; i++)
			Dat2Buf[i] = (BYTE)0x90;

		if (!WriteMem(processInformation.hProcess, DatFix2, Dat2Buf, 12))
		{
			MessageBoxA(0, "Could not Write Dat Fix #2.", "GWMC - Err", 0);
			delete scan;
			delete Dat2Buf;
			return -1;
		}

		// CLEAN UP
		delete[] Dat2Buf;
	}

	delete scan;

	//Load DLL if it exists.
	if (GetPrivateProfileStringA("GWMC", "DLL", "", DLLName, 100, INIPath)){
		LPVOID pStringInRemoteProcess;
		pStringInRemoteProcess = VirtualAllocEx(processInformation.hProcess, 0, strlen(DLLName) + 1, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
		WriteProcessMemory(processInformation.hProcess, pStringInRemoteProcess, DLLName, strlen(DLLName) + 1, NULL);
		HANDLE hThread = CreateRemoteThread(processInformation.hProcess, 0, 0, (LPTHREAD_START_ROUTINE)GetProcAddress(GetModuleHandleA("kernel32.dll"), "LoadLibraryA"), pStringInRemoteProcess, 0, 0);
		WaitForSingleObject(hThread, INFINITE);
		VirtualFreeEx(processInformation.hProcess, pStringInRemoteProcess, strlen(DLLName) + 1, MEM_FREE);
		CloseHandle(hThread);
	}
	// Continue like nothing happened.
	ResumeThread(processInformation.hThread);

	return EXIT_SUCCESS;
}