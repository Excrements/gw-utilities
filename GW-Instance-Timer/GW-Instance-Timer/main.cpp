#include <Windows.h>
#include "GameContext.h"
#include "DXHooker_gw.h"

#include <d3d9\include\d3dx9.h>

using EndScene_t = HRESULT(WINAPI*)(IDirect3DDevice9* dev);
using Reset_t	 = HRESULT(WINAPI*)(IDirect3DDevice9* dev,D3DPRESENT_PARAMETERS* params);

GWDXHooker dx_hook;
GWCA::GameContext* game = nullptr;
ID3DXFont* lefont = nullptr;
ID3DXFont* lebgfont = nullptr;

const LONG x = 50, y = 50;
RECT fontrect = { x, y, x + 300, y + 48 };
RECT fontrect2 = { x+2, y+2, x+2 + 300, y+2 + 48 };

static void DrawRectangleAlpha(IDirect3DDevice9* pDevice, float x, float y, float width, float height, DWORD color)
{
	struct Vertex
	{
		float x, y, z, rhw;
		DWORD color;
	};
	Vertex qV[4] = { { x, y + height, 0.0f, 0.0f, color },
	{ x, y, 0.0f, 0.0f, color },
	{ x + width,y + height, 0.0f, 0.0f, color },
	{ x + width,y, 0.0f, 0.0f, color } };

	pDevice->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, qV, sizeof(Vertex));
}


HRESULT WINAPI dx_endscene(IDirect3DDevice9* dev)
{
	static char txt_buf[32];
	IDirect3DStateBlock9* stateBlock = NULL;
	dev->CreateStateBlock(D3DSBT_ALL, &stateBlock);
	dev->SetRenderState(D3DRS_ALPHABLENDENABLE, 1);
	dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
	dev->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
	dev->SetFVF(D3DFVF_XYZRHW | D3DFVF_DIFFUSE);


	int ms = game->agent->instancetimer;
	int sec = (ms / 1000) % 60;
	int min = (ms / 60000) % 60;
	int hr = ms / 3600000;

	sprintf_s(txt_buf, "%d:%02d:%02d", hr, min, sec);

	lebgfont->DrawTextA(nullptr, txt_buf, -1, &fontrect2, DT_LEFT, 0xFF000000);
	lefont->DrawTextA(nullptr, txt_buf, -1, &fontrect, DT_LEFT, 0xFFFF00FF);

	stateBlock->Apply();
	stateBlock->Release();

	return dx_hook.original_function<EndScene_t>(dx9::kEndScene)(dev);
}


HRESULT WINAPI dx_reset(IDirect3DDevice9* dev, D3DPRESENT_PARAMETERS* params)
{

	lefont->OnLostDevice();
	lebgfont->OnLostDevice();

	HRESULT result = dx_hook.original_function<Reset_t>(dx9::kReset)(dev, params);
	if (result == D3D_OK) {

		lefont->OnResetDevice();
		lebgfont->OnResetDevice();

	}
	return result;
}



BOOL WINAPI DllMain(HMODULE hMod, DWORD dwReason, LPVOID)
{
	if (dwReason == DLL_PROCESS_ATTACH) {

		while ((game = GWCA::GameContext::instance()) == nullptr) Sleep(100);

		dx_hook.Initialize();

		D3DXCreateFont(dx_hook.device(), 48, 0, FW_BOLD, 0, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial", &lefont);
		D3DXCreateFont(dx_hook.device(), 48, 0, FW_BOLD, 0, false, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "Arial", &lebgfont);

		dx_hook.AddHook(dx9::kEndScene, dx_endscene);
		dx_hook.AddHook(dx9::kReset, dx_reset);
	}
	else if (dwReason == DLL_PROCESS_DETACH) {
		dx_hook.Restore();
	}
	return TRUE;
}