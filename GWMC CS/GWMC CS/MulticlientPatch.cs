﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using GWCA.Memory;

namespace GWMC_CS
{
    class MulticlientPatch
    {
        GWCAMemory mem;

        public MulticlientPatch(GWCAMemory memorymgr)
        {
            mem = memorymgr;
            mem.InitScanner(new IntPtr(0x401000), 0x49A000);
        }

        ~MulticlientPatch()
        {
            mem.TerminateScanner();
        }

        public bool ApplyMulticlientPatch()
        {
            IntPtr mcmutexaddr = mem.ScanForPtr( new byte[]{ 0x8B, 0xF0, 0x85, 0xF6, 0x74, 0x10, 0xFF }, 0x14);
            if (mcmutexaddr == IntPtr.Zero) return false;

            mem.WriteBytes(mcmutexaddr, new byte[] { 0xEB });

            return true;
        }

        public bool ApplyDatFix()
        {
            IntPtr datfix1 = mem.ScanForPtr( new byte[]{ 0x6A, 0x00, 0xBA, 0x00, 0x00, 0x00, 0xC0 }, 0x1);
            if (datfix1 == IntPtr.Zero) return false;

            mem.WriteBytes(datfix1, new byte[] { 0x03 });

            IntPtr datfix2 = mem.ScanForPtr(new byte[] { 0x6A, 0x00, 0x52, 0x57, 0x50, 0x51 }, 0x0);
            if (datfix2 == IntPtr.Zero) return false;

            mem.WriteBytes(datfix2, new byte[] { 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90 });

            return true;
        }
    }
}
