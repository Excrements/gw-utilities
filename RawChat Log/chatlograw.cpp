#include <Windows.h>
#include <cstdio>

#define CHAT_LOG_ADDR (BYTE*)0x59B4E0
#define CHARACTER_NAME (wchar_t*)0xA2AE88

wchar_t windowbuffer[48];


BYTE *func, *funcret;

void *DetourFunc(BYTE *src, const BYTE *dst, const int len)
{


	BYTE *jmp = (BYTE*)malloc(len + 5);
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_READWRITE, &dwBack);

	memcpy(jmp, src, len);
	jmp += len;

	jmp[0] = 0xE9;
	*(DWORD*)(jmp + 1) = (DWORD)(src + len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src + 1) = (DWORD)(dst - src) - 5;

	for (int i = 5; i < len; i++)
		src[i] = 0x90;

	VirtualProtect(src, len, dwBack, &dwBack);

	return (jmp - len);
}

void RetourFunc(BYTE *src, BYTE *restore, const int len)
{
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_READWRITE, &dwBack);
	memcpy(src, restore, len);

	restore[0] = 0xE9;
	*(DWORD*)(restore + 1) = (DWORD)(src - restore) - 5;

	VirtualProtect(src, len, dwBack, &dwBack);

	free(restore);
}

wchar_t currentstringbuff[0x200];

void __stdcall Chathandler(wchar_t* chat){

	wcscpy(currentstringbuff, chat);
#ifdef _DEBUG
	wprintf(L"%s\n", chat);
#endif
	;
	HWND win = FindWindowW(NULL, windowbuffer);

	if (win)
		PostMessageA(win, 0x508, (WPARAM)currentstringbuff, NULL);
}

void __declspec(naked) detourfuncchat(){
	_asm {
		PUSHAD
		CMP EDX, 0x8A1858
		JNE DETOURFUNCRETURN
		PUSH ECX
		CALL Chathandler
	DETOURFUNCRETURN:
		POPAD
		JMP funcret
	}
}

void init(HMODULE hModule){
	wsprintf(windowbuffer, L"RAWCHAT - %s", CHARACTER_NAME);


#ifdef _DEBUG
	AllocConsole();
	FILE* fh;
	freopen_s(&fh, "CONOUT$", "w", stdout);
#endif

	funcret = (BYTE*)DetourFunc(CHAT_LOG_ADDR, (BYTE*)detourfuncchat, 5);

}


BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved){
	if (_Reason == DLL_PROCESS_ATTACH){
		DisableThreadLibraryCalls(_HDllHandle);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
	}
	return TRUE;
}