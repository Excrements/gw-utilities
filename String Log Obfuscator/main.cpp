#include <Windows.h>

void *DetourFunc(BYTE *src, const BYTE *dst, const int len)
{

	BYTE *jmp = (BYTE*)malloc(len + 5);
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_READWRITE, &dwBack);

	memcpy(jmp, src, len);
	jmp += len;

	jmp[0] = 0xE9;
	*(DWORD*)(jmp + 1) = (DWORD)(src + len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src + 1) = (DWORD)(dst - src) - 5;

	for (int i = 5; i < len; i++)
		src[i] = 0x90;

	VirtualProtect(src, len, dwBack, &dwBack);

	return (jmp - len);
}

void RetourFunc(BYTE *src, BYTE *restore, const int len)
{
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_READWRITE, &dwBack);
	memcpy(src, restore, len);

	restore[0] = 0xE9;
	*(DWORD*)(restore + 1) = (DWORD)(src - restore) - 5;

	VirtualProtect(src, len, dwBack, &dwBack);

	free(restore);
}

BYTE* func;
BYTE* funcret;

void __fastcall handler(wchar_t* name)
{

	for (size_t i = 0; i < wcslen(name); ++i)
		name[i] = '?';

}

void __declspec(naked) detourStringLog(){
	_asm{
		PUSHAD
		CALL handler
		POPAD
		JMP funcret
	}
}

BYTE* GetStringLogAddr(){
	const BYTE StringLogCode[] = { 0x89,0x3E,0x8B,0x7D,0x10,0x89,0x5E,0x04,0x39,0x7E,0x08 };

	for (BYTE* i = (BYTE*)0x401000; i < (BYTE*)0x900000; ++i){

		if (!memcmp(i, StringLogCode, sizeof(StringLogCode)))
			return i + 0x22;

	}

	return NULL;
}


void init(HMODULE hModule){

	func = GetStringLogAddr();
	if (!func) return;

	funcret = (BYTE*)DetourFunc(func, (BYTE*)detourStringLog, 6);

	while (1){
		Sleep(100);
		if (GetAsyncKeyState(VK_END) & 1){
			RetourFunc(func, (BYTE*)funcret, 6);
			FreeLibraryAndExitThread(hModule, 0);
		}
	}

}


BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved){
	if (_Reason == DLL_PROCESS_ATTACH){
		DisableThreadLibraryCalls(_HDllHandle);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
	}
	return TRUE;
}