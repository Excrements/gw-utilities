#include <Windows.h>
#include "ConsoleEx.h"

ConsoleEx* con;

void *DetourFunc(BYTE *src, const BYTE *dst, const int len, BYTE** restore = NULL)
{

	if (restore){
		*restore = new BYTE[len];
		memcpy(*restore, src, len);
	}


	BYTE *jmp = (BYTE*)malloc(len + 5);
	DWORD dwBack;
	VirtualProtect(jmp, len+5, PAGE_EXECUTE_READWRITE, &dwBack);
	VirtualProtect(src, len, PAGE_EXECUTE_READWRITE, &dwBack);

	memcpy(jmp, src, len);
	jmp += len;

	jmp[0] = 0xE9;
	*(DWORD*)(jmp + 1) = (DWORD)(src + len - jmp) - 5;

	src[0] = 0xE9;
	*(DWORD*)(src + 1) = (DWORD)(dst - src) - 5;

	for (int i = 5; i < len; i++)
		src[i] = 0x90;

	VirtualProtect(src, len, dwBack, &dwBack);

	return (jmp - len);
}

void RetourFunc(BYTE *src, BYTE *restore, const int len)
{
	DWORD dwBack;

	VirtualProtect(src, len, PAGE_EXECUTE_READWRITE, &dwBack);
	memcpy(src, restore, len);

	restore[0] = 0xE9;
	*(DWORD*)(restore + 1) = (DWORD)(src - restore) - 5;

	VirtualProtect(src, len, dwBack, &dwBack);

	delete[] restore;
}


struct P37_KeyboardUpdate{
	const DWORD Header = 0x37;
	float CurrentX;
	float CurrentY;
	DWORD ZPlane;
	float XOffset;
	float YOffset;
	DWORD MoveDirection;
};

struct P40_KeyboardStop{
	const DWORD Header = 0x37;
	float CurrentX;
	float CurrentY;
	DWORD ZPlane;
};

struct P80_LoadEquipment{
	DWORD head;
	short party;
	short dos;
	short tres[4];
	char quatro;
	char cinco;
	char seis;
};


typedef void(__fastcall *PacketSend_t)(DWORD* loc, DWORD size, DWORD* packet);

BYTE* paksendfunc = NULL, *paksendrestore = NULL;
PacketSend_t paksendret = NULL;

void __fastcall detourPacketSend(DWORD* loc, DWORD size, DWORD* packet)
{
	con->printf("<CtoGS> Size: 0x%X Header: 0x%X\n", size, *packet);

	
		for (DWORD i = 1; i < (0.25 * size); i++)
		{
			con->printf("0x%X ", packet[i]);
		}

	con->printf("\n---------------------------------\n");

	return paksendret(loc, size, packet);
}


void init(HMODULE hModule){
	con = new ConsoleEx(hModule);
	CONSOLEEX_PARAMETERS params = ConsoleEx::GetDefaultParameters();
	params.closemenu = true;

	con->create("CtoGS Packet Log - 4D 1 @ gamerevision.com", &params);

	const byte CtoSPacketSendCode[] = { 0x2C, 0x53, 0x56, 0x57, 0x8B, 0xF9, 0x85, 0xFF, 0x8B, 0xDA };

	for (BYTE* scan = (BYTE*)0x401000; scan < (BYTE*)0x900000; scan++)
	{
		if (!memcmp(scan, CtoSPacketSendCode, sizeof(CtoSPacketSendCode)))
		{
			paksendfunc = scan - 5;
			paksendret = (PacketSend_t)DetourFunc(paksendfunc, (BYTE*)detourPacketSend, 6, &paksendrestore);
			break;
		}
	}

	if (!paksendfunc)
		con->printf("Packet Send Function not found, bytecode changed?");
	else
		con->printf("Packet send func found at %X. Now logging...\n", paksendfunc);

	while (con->isOpen()) Sleep(100);

	RetourFunc(paksendfunc, paksendrestore, 6);
	FreeLibraryAndExitThread(hModule, EXIT_SUCCESS);
}


BOOL WINAPI DllMain(_In_ HMODULE _HDllHandle, _In_ DWORD _Reason, _In_opt_ LPVOID _Reserved){
	if (_Reason == DLL_PROCESS_ATTACH){
		DisableThreadLibraryCalls(_HDllHandle);
		CreateThread(0, 0, (LPTHREAD_START_ROUTINE)init, _HDllHandle, 0, 0);
	}
	return TRUE;
}

