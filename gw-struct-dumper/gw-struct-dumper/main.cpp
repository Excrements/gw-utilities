
#define _CRT_SECURE_NO_WARNINGS

#include <Windows.h>
#include <cstdio>
#include "packetparameterinfo.h"

void output_param(const PacketParameter& param,int param_index) {
		switch (param.type) {
		case ParamType::kUInteger:
			switch (param.size) {
			case 1:
				printf("\tuint8_t param%02d_;\n", param_index);
				break;
			case 2:
				printf("\tuint16_t param%02d_;\n", param_index);
				break;
			case 4:
				printf("\tuint32_t param%02d_;\n", param_index);
				break;
			default:
				printf("\t<unknown-uint> param%02d_;\n", param_index);
				break;
			}
			break;
		case ParamType::kElementCount:
			printf("\tuint32_t param%02dcount_;\n", param_index-1);
			break;
		case ParamType::kAgentId:
			printf("\tAgentID param%02d_;\n", param_index);
			break;
		case ParamType::kVector2f:
			printf("\tVector2f param%02d_;\n", param_index);
			break;
		case ParamType::kVector3f:
			printf("\tVector3f param%02d_;\n", param_index);
			break;
		case ParamType::kArrayWChar:
			printf("\twchar_t param%02d_[%d];\n", param_index, param.size);
			break;
		case ParamType::kArrayUInt32:
			printf("\tuint32_t param%02d_[%d];\n", param_index, param.size);
			break;
		case ParamType::kArrayUInt8:
			printf("\tuint8_t param%02d_[%d];\n", param_index, param.size);
			break;
		case ParamType::kArayUInt16:
			printf("\tuint16_t param%02d_[%d];\n", param_index, param.size);
			break;
		default:
			printf("\t<unknown-type> param%02d_;\n", param_index);
			break;
		}
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved)
{
	if (dwReason == DLL_PROCESS_ATTACH) {

		char dll_path[MAX_PATH];
		DWORD dll_path_len = GetModuleFileNameA(hModule, dll_path, MAX_PATH);
		char* accessor = dll_path + dll_path_len;
		while (*accessor != '\\') --accessor;
		memcpy(accessor, "\\ctogs_def.txt", sizeof("\\ctogs_def.txt"));

		freopen(dll_path, "w", stdout);

		printf("struct Packet {\n\tHeader header_;\n};\n\n");


		auto& ctos = gs_obj->constdata->ctostable;

		for (int i = 0; i < ctos.size(); ++i)
		{
			try {
				auto& index = ctos[i];

				if (index.params == nullptr || index.paramcount == 0) continue;


				printf("struct Packet%04X : public Packet {\n", i);

				for (int j = 1; j < index.paramcount; ++j) {
					output_param(index.params[j],j);
				}

			}
			catch (gw_array<CtoSTableIndex>::Exception e) {
				printf("ERROR: CtoGS Array Index[%d]: %s", i, e.msg());
			}

			printf("};\n\n");
		}

		auto& stoc = gs_obj->constdata->stoctable;

		memcpy(accessor, "\\gstoc_def.txt", sizeof("\\gstoc_def.txt"));
		freopen(dll_path, "w", stdout);


		for (int i = 0; i < stoc.size(); ++i)
		{
			try {
				auto& index = stoc[i];

				if (index.params == nullptr || index.paramcount == 0) continue;


				printf("struct Packet%04X {\n", i);
				printf("\tuint32_t header = 0x%X;\n", index.params[0].header);

				for (int j = 1; j < index.paramcount; ++j) {
					output_param(index.params[j],j);
				}

			}
			catch (gw_array<StoCTableIndex>::Exception e) {
				printf("ERROR: GStoC Array Index[%d]: %s", i, e.msg());
			}

			printf("};\n\n");
		}

		memcpy(accessor, "\\ctols_def.txt", sizeof("\\ctols_def.txt"));
		freopen(dll_path, "w", stdout);

		auto& ctols = gs_obj->constdata->ls->ctos_table;

		for (int i = 0; i < ctols.size(); ++i)
		{
			try {
				auto& index = ctols[i];

				if (index.params == nullptr || index.paramcount == 0) continue;


				printf("struct Packet%04X {\n", i);
				printf("\tuint32_t header = 0x%X;\n", index.params[0].header);

				for (int j = 1; j < index.paramcount; ++j) {
					output_param(index.params[j], j);
				}

			}
			catch (gw_array<CtoSTableIndex>::Exception e) {
				printf("ERROR: CtoGS Array Index[%d]: %s", i, e.msg());
			}

			printf("};\n\n");
		}

		memcpy(accessor, "\\lstoc_def.txt", sizeof("\\lstoc_def.txt"));
		freopen(dll_path, "w", stdout);

		auto& lstoc = gs_obj->constdata->ls->stoc_table;
		for (int i = 0; i < ctols.size(); ++i)
		{
			try {
				auto& index = lstoc[i];

				if (index.params == nullptr || index.paramcount == 0) continue;


				printf("struct Packet%04X {\n", i);
				printf("\tuint32_t header = 0x%X;\n", index.params[0].header);

				for (int j = 1; j < index.paramcount; ++j) {
					output_param(index.params[j], j);
				}

			}
			catch (gw_array<StoCTableIndex>::Exception e) {
				printf("ERROR: CtoGS Array Index[%d]: %s", i, e.msg());
			}

			printf("};\n\n");
		}

		freopen("CONOUT$", "w", stdout);

	}
	return FALSE;
}
