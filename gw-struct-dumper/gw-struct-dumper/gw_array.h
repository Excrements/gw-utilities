#pragma once


template <typename T>
class gw_array {
public:

	class Exception {
	public:
		virtual const char* msg() { return ""; }
	};

	class OutOfBoundsException : public Exception {
		const char* msg() override { return "Index Out of Bounds"; }
	};

	class NullArrayException : public Exception {
		const char* msg() override { return "Index array is null"; }
	};

	T& operator[](unsigned int index) {
		if (index >= sizecurrent_) throw OutOfBoundsException();
		if (!array_) throw NullArrayException();
		return array_[index];
	}

	unsigned int size() const { return sizecurrent_; }

private:
	T* array_;
	unsigned int sizeallocated_;
	unsigned int sizecurrent_;
	unsigned int unk_;
};