#pragma once

#include <cstdint>
#include "gw_array.h"

#pragma pack(1)
using StoCHandler = bool(__fastcall*)(void*);

enum ParamType : uint8_t {
	kUInteger = 0x4,
	kArrayUInt8 = 0x5,
	kElementCount = 0x6,
	kArayUInt16 = 0xB,
	kAgentId = 0x10,
	kVector2f = 0x12,
	kVector3f = 0x13,
	kArrayWChar = 0x17,
	kArrayUInt32 = 0x2B
};

// First param = header, rest are encoded types.
union PacketParameter {
	uint32_t header;
	struct {
		ParamType type; // Type of parameter.
		uint16_t size; // Size of parameter if required (byte size for primitives,element count for arrays).
		uint8_t unk1; // Might not even be there and its just default padding
	};
};
struct CtoSTableIndex{
	PacketParameter* params;
	uint32_t paramcount;
};
struct StoCTableIndex{
	PacketParameter* params;
	uint32_t paramcount;
	StoCHandler handler;
};

struct GameServer {
	char pad1[0x8];
	struct sub1{
		char pad1[0xC];
		struct {
			char pad1[0x1C];
			gw_array<CtoSTableIndex> ctos_table;
			gw_array<StoCTableIndex> stoc_table;
		}*ls;
		char pad2[0xC];
		gw_array<CtoSTableIndex> ctostable;
		gw_array<StoCTableIndex> stoctable;
	}*constdata;
};
#pragma pack()

// 6A 02 8D 55 F8 C7 45 F8 03 00 00 00 (-4)
GameServer* gs_obj = *(GameServer**)0xA2B294;